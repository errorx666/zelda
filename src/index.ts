import { AmbientLight, Mesh, PlaneGeometry, MeshBasicMaterial, OrthographicCamera, Scene, WebGLRenderer, LinearEncoding, ImageBitmapLoader, CanvasTexture, NearestMipMapNearestFilter, sRGBEncoding } from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader';
import { GammaCorrectionShader } from 'three/examples/jsm/shaders/GammaCorrectionShader';

let stats: any;
if( ( String( process.env.NODE_ENV ) === 'development' ) ) {
	const Stats = require( 'stats.js' );
	stats = new Stats;
	document.body.appendChild( stats.dom );
	stats.dom.style.left = '';
	stats.dom.style.right = '0';
}

const canvas = document.querySelector( 'canvas' );
const { width, height } = canvas;
const contextOptions = {
	alpha: true,
	antialias: true,
	depth: true,
	desynchronized: false,
	premultipliedAlpha: true,
	preserveDrawingBuffer: false,
	stencil: true
};
const context = canvas.getContext( 'webgl2', contextOptions );
const renderer = new WebGLRenderer( {
	...contextOptions,
	canvas,
	context: context as WebGLRenderingContext,
	precision: 'mediump'
} );
renderer.setPixelRatio( 1 );
renderer.debug.checkShaderErrors = ( String( process.env.NODE_ENV ) === 'development' );
renderer.outputEncoding = LinearEncoding;
renderer.setClearColor( 0x000033 );

const imageBitmapLoader = new ImageBitmapLoader;
async function loadImage( src: string ) {
	const url: string = ( await import( `~maps/${src}` ) ).default;
	const imageBitmap = await new Promise<ImageBitmap>( ( resolve, reject ) => {
		imageBitmapLoader.load( url, resolve, null, reject );
	} );
	const map = new CanvasTexture( imageBitmap as any );
	map.generateMipmaps = true;
	map.magFilter = NearestMipMapNearestFilter;
	map.minFilter = NearestMipMapNearestFilter;
	map.encoding = sRGBEncoding;

	const material = new MeshBasicMaterial( {
		dithering: true,
		fog: false,
		map
	} );

	return material;
}

const camera = new OrthographicCamera( width * -.5, width * .5, height * -.5, height * .5, 1, 100 );
const scene = new Scene;
scene.add( camera );
const composer = new EffectComposer( renderer );
const renderPass = new RenderPass( scene, camera );
const fxaaPass = new ShaderPass( FXAAShader );
fxaaPass.uniforms[ 'resolution' ].value.x = 1 / ( width * renderer.getPixelRatio() );
fxaaPass.uniforms[ 'resolution' ].value.y = 1 / ( height * renderer.getPixelRatio() );
composer.addPass( fxaaPass );

const gammaCorrectionPass = new ShaderPass( GammaCorrectionShader );
composer.addPass( renderPass );
composer.addPass( gammaCorrectionPass );

( async () => {
	scene.add( new AmbientLight( 0xffffff, 1 ) );
	const geom = new PlaneGeometry( width * 7, height * 12, 1, 1 );
	const material = await loadImage( 'bg-overworld-light.png' );
	const mesh = new Mesh( geom, material );
	scene.add( mesh );

	camera.position.set( 0, 0, -1 );
	camera.lookAt( mesh.position );
	camera.updateProjectionMatrix();

	let lastTime = performance.now();
	renderer.setAnimationLoop( () => {
		const time = performance.now();
		const deltaTime = time - lastTime;
		lastTime = time;
		if( ( String( process.env.NODE_ENV ) === 'development' ) ) {
			stats.begin();
		}

		camera.position.setX( Math.sin( time * .0005 ) * 512 );
		camera.position.setY( Math.cos( time * .0005 ) * 512 );

		composer.render( deltaTime );
		if( ( String( process.env.NODE_ENV ) === 'development' ) ) {
			stats.end();
		}
	} );
} )();
