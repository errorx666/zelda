export type Entity = symbol & { readonly __entity: unique symbol; };

export interface Component<TKey extends string> {
	readonly key: TKey;
	entity: Entity;
}

interface ComponentTypeMap {
	[ key: string ]: Component<typeof key>;
}

export class Ecs {
	private readonly _components = Object.create( null, {} ) as {
		[ TKey in keyof ComponentTypeMap ]?: ( ComponentTypeMap[ TKey ] )[];
	};

	private readonly _entityComponents = new Map<Entity, Map<keyof ComponentTypeMap, Component<string>>>();

	public get<TKey extends keyof ComponentTypeMap>( entity: Entity, key: TKey ) {
		return ( this._entityComponents.get( entity )?.get( key ) ?? null ) as ComponentTypeMap[ TKey ];
	}

	public getAll<TKey extends keyof ComponentTypeMap>( key: TKey ) {
		return this._components[ key ] ?? [];
	}

	public addComponent<TKey extends keyof ComponentTypeMap>( entity: Entity, component: ComponentTypeMap[ TKey ] ) {
		component.entity = entity;
		const compArray = this._components[ component.key ];
		if( compArray == null ) {
			this._components[ component.key ] = [ component ];
		} else {
			compArray.push( component );
		}
		this._entityComponents.get( entity ).set( component.key, component );
	}

	private _removeFromComponents<TKey extends keyof ComponentTypeMap>( component: ComponentTypeMap[ TKey ] ) {
		component.entity = null;
		this._components[ component.key ] = this._components[ component.key ].filter( e => e !== component );
	}

	public removeComponent<TKey extends keyof ComponentTypeMap>( component: ComponentTypeMap[ TKey ] ) {
		this._entityComponents.get( component.entity ).delete( component.key );
		this._removeFromComponents( component );
	}

	public createEntity() {
		const entity = Symbol( 'entity' ) as Entity;
		this._entityComponents.set( entity, new Map );
		return entity;
	}

	public deleteEntity( entity: Entity ) {
		for ( const component of this._entityComponents.get( entity ).values() ) {
			this._removeFromComponents( component );
		}
		this._entityComponents.delete( entity );
	}
}
