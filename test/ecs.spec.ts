import { describe, it } from 'mocha';
import { expect } from 'chai';

import { Ecs } from '~ecs';

describe( 'ecs', async () => {
	describe( 'addComponent', async () => {
		it( 'sets the entity property of a component', async () => {
			const ecs = new Ecs;
			const entity = ecs.createEntity();
			const component = { key: 'test', entity: null };
			ecs.addComponent( entity, component );
			expect( component.entity ).to.equal( entity );
		} );
	} );
	describe( 'createEntity', async () => {
		it( 'returns a unique symbol', async () => {
			const ecs = new Ecs;
			const entity1 = ecs.createEntity();
			const entity2 = ecs.createEntity();
			expect( entity1 ).to.be.a( 'symbol' );
			expect( entity2 ).to.be.a( 'symbol' );
			expect( entity1 ).not.to.equal( entity2 );
		} );
	} );
	describe( 'deleteEntity', async () => {
		it( 'removes all components from an entity', async () => {
			const ecs = new Ecs;
			const entity = ecs.createEntity();
			const component1 = { key: 'test1', entity: null };
			const component2 = { key: 'test2', entity: null };
			ecs.addComponent( entity, component1 );
			ecs.addComponent( entity, component2 );
			ecs.deleteEntity( entity );
			expect( ecs.get( entity, component1.key ) ).to.equal( null );
			expect( ecs.get( entity, component2.key ) ).to.equal( null );
		} );
		it( 'sets the entity property of all components to null', async () => {
			const ecs = new Ecs;
			const entity = ecs.createEntity();
			const component1 = { key: 'test1', entity: null };
			const component2 = { key: 'test2', entity: null };
			ecs.addComponent( entity, component1 );
			ecs.addComponent( entity, component2 );
			ecs.deleteEntity( entity );
			expect( component1.entity ).to.equal( null );
			expect( component2.entity ).to.equal( null );
		} );
	} );
	describe( 'get', async () => {
		it( 'returns the associated component', async () => {
			const ecs = new Ecs;
			const entity = ecs.createEntity();
			const component1 = { key: 'test1', entity: null };
			const component2 = { key: 'test2', entity: null };
			ecs.addComponent( entity, component1 );
			expect( ecs.get( entity, component1.key ) ).to.equal( component1 );
			ecs.addComponent( entity, component2 );
			expect( ecs.get( entity, component2.key ) ).to.equal( component2 );
		} );
		it( 'returns null if the component does not exist', async () => {
			const ecs = new Ecs;
			const entity = ecs.createEntity();
			expect( ecs.get( entity, 'fail' ) ).to.equal( null );
		} );
	} );
	describe.skip( 'getAll', async () => {
// TODO
	} );
	describe( 'removeComponent', async () => {
		it( 'sets the entity property of a component to null', async () => {
			const ecs = new Ecs;
			const entity = ecs.createEntity();
			const component = { key: 'test', entity: null };
			ecs.addComponent( entity, component );
			ecs.removeComponent( component );
			expect( component.entity ).to.be.null;
		} );
	} );
} );
