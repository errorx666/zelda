const webpack = require( 'webpack' );
const { CheckerPlugin } = require( 'awesome-typescript-loader' );
const CleanObsoleteChunksPlugin = require( 'webpack-clean-obsolete-chunks' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const TsconfigPathsWebpackPlugin = require( 'tsconfig-paths-webpack-plugin' );
const path = require( 'path' );

/** @type {import('webpack').Configuration} */
module.exports = {
	devtool: 'source-map',
	devServer: {
		contentBase: path.join( __dirname, 'dist' ),
		compress: true,
		disableHostCheck: true,
		hot: true,
		host: '0.0.0.0',
		port: 8082,
		stats: 'minimal'
	},
	entry: {
		index: [
			path.resolve( __dirname, 'src', 'index.css' ),
			path.resolve( __dirname, 'src', 'index' )
		]
	},
	mode: process.env.NODE_ENV,
	module: {
		rules: [
			{	test: /\.ts$/i,
				include: [ path.resolve( __dirname, 'src' ) ],
				use: [ { loader: 'awesome-typescript-loader', options: {
					babelCore: '@babel/core',
					babelOptions: {
						presets: [ [ '@babel/preset-env', {
							corejs: 3,
							modules: false,
							useBuiltIns: 'usage',
							shippedProposals: true
						} ] ]
					},
					useBabel: true,
					useCache: true
				} } ]
			},
			{ test: /\.(?:bmp|gif|jpe?g|png)$/i, use: [ { loader: 'file-loader' } ] },
			{ test: /\.css$/i, use: [ MiniCssExtractPlugin.loader, { loader: 'css-loader' } ] }
		]
	},
	output: {
		path: path.resolve( __dirname, 'dist' ),
		filename: '[name].js'
	},
	node: {
		global: true,
		__dirname: true,
		__filename: true
	},
	plugins: [
		new CheckerPlugin,
		new HtmlWebpackPlugin( {
			inject: 'body',
			template: path.resolve( __dirname, 'src', 'index.html' ),
			templateParameters: {}
		} ),
		new MiniCssExtractPlugin( { filename: '[hash].css' } ),
		new CleanObsoleteChunksPlugin,
		new webpack.ProvidePlugin( {
			THREE: 'three'
		} ),
		new webpack.DefinePlugin( {
			'process.browser': JSON.stringify( true ),
			'process.platform': JSON.stringify( 'browser' )
		} )
	],
	resolve: {
		alias: {
			'~maps': path.resolve( __dirname, 'src', 'maps' )
		},
		extensions: [ '.ts', '.mjs', '.cjs', '.js', '.json' ],
		plugins: [
			new TsconfigPathsWebpackPlugin
		]
	}
};
